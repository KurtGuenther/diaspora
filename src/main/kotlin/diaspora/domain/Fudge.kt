package diaspora.domain

enum class Fudge(val value: Int) {
    None(-4),
    Terrible(-3),
    Poor(-2),
    Mediocre(-1),
    Fair(0),
    Good(1),
    Great(2),
    Excellent(3),
    Superb(4);


    fun boost(amount: Int = 1): Fudge {
        val i = this.ordinal + amount
        return if (i >= Superb.ordinal) Superb else values()[i]
    }

    fun diminish(amount: Int = 1): Fudge = boost(-amount)

}
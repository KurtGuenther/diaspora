package diaspora.domain

import java.util.*

data class SlipStream(
        val id: String = UUID.randomUUID().toString(),
        val starPair: Pair<StarSystem, StarSystem>
)
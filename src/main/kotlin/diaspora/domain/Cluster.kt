package diaspora.domain

import java.util.*

/**
 * A cluster holds a number of system and also the slipstreams that allow for
 * FTL between star systems.
 */
data class Cluster(
        val id: String? = null,
        val timestamp: Date? = null,
        val name: String,
        val notes: String? = null,
        val starSystems: Set<StarSystem>? = null,
        val slipStreams: Set<SlipStream>? = null
) {

    // FUTURE:  generate this boilerplate with Kapt
    fun toBuilder() = Builder(
            id,
            timestamp,
            name,
            notes,
            starSystems,
            slipStreams
    )

    class Builder(
            var id: String? = null,
            var timestamp: Date? = null,
            var name: String? = null,
            var notes: String? = null,
            var starSystems: Set<StarSystem>? = null,
            var slipStreams: Set<SlipStream>? = null
    ) {

        fun build(): Cluster = Cluster(
                id,
                timestamp,
                name ?: throw Exception("Name is required"),
                notes,
                starSystems,
                slipStreams
        )

        fun id(id: String): Builder {
            this.id = id
            return this
        }

        fun timestamp(timestamp: Date?): Builder {
            this.timestamp = timestamp
            return this
        }

        fun name(name: String): Builder {
            this.name = name
            return this
        }

        fun notes(notes: String): Builder {
            this.notes = notes
            return this
        }

        fun starSystems(starSystems: Set<StarSystem>): Builder {
            this.starSystems = starSystems
            return this
        }

        fun slipStreams(slipStreams: Set<SlipStream>): Builder {
            this.slipStreams = slipStreams
            return this
        }
    }
}

package diaspora.domain

import java.util.*

data class Player(
        val id: String? = null,
        val timestamp: Date? = null,
        val name: String,
        val notes: String? = null,
        val systemIds: List<String>? = null
) {

    // FUTURE: this boiler plate should be generated with kapt
    fun toBuilder() = Builder(id, timestamp, name, notes, systemIds)

    class Builder(
            var id: String? = null,
            var timestamp: Date? = null,
            var name: String? = null,
            var notes: String? = null,
            var systemIds: List<String>? = null
    ) {

        fun build() = Player(
                id,
                timestamp,
                name ?: throw Exception("name cannot be null"),
                notes,
                systemIds
        )

        fun setId(id: String?): Builder {
            this.id = id
            return this
        }

        fun setTimeStamp(timestamp: Date?): Builder {
            this.timestamp = timestamp
            return this
        }

        fun setName(name: String): Builder {
            this.name = name
            return this
        }

        fun setNotes(notes: String): Builder {
            this.notes = notes
            return this
        }

        fun setSystemIds(systemIds: List<String>): Builder {
            this.systemIds = systemIds
            return this
        }

    }
}



package diaspora.domain

import java.sql.Timestamp

data class Planet(
        val id: String? = null,
        val timestamp: Timestamp? = null,
        val name: String,
        val notes: String,
        val habitability: Fudge
)
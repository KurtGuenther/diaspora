package diaspora.domain

import diaspora.utils.Common.generateId
import java.util.*

data class StarSystem(
        val id: String,
        val name: String,
        val notes: String,
        val statistics: String,
        val aspects: String,
        val details: String,
        val planets: List<Planet>,  // ordered from inner to outer planets.
        val technology: Fudge,
        val environment: Fudge,
        val resources: Fudge
)



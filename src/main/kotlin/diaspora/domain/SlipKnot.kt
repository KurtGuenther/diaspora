package diaspora.domain

data class SlipKnot(
        val id: String,
        val pointInSpace: String
)

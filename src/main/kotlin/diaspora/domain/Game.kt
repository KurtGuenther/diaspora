package diaspora.domain

import java.sql.Timestamp

data class Game(
        val id: String? = null,
        val timestamp: Timestamp? = null,
        val name: String,
        val notes: String?,
        val players: List<Player>,
        val cluster: Cluster,
        val caller: Player?,
        val referee: Player?
)


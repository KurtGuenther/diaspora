package diaspora.controller

import diaspora.service.NamingService
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get


@Controller("/diaspora/naming")
class NamingController(val namingService: NamingService) {

    class Name(val name: String)  // DTO

    @Get("/cluster")
    fun clusterName() = Name(namingService.clusterName())

    @Get("/system")
    fun systemName() = Name(namingService.systemName())

}

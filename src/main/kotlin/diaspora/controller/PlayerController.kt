package diaspora.controller

import diaspora.domain.Player
import diaspora.service.PlayerService
import io.micronaut.http.annotation.*
import mu.KotlinLogging

private val log = KotlinLogging.logger {}

@Controller("/diaspora/player")
class PlayerController(val playerService: PlayerService) {

    @Get("/{id}")
    fun find(@PathVariable id: String): Player {
        log.debug("find player with id = $id")
        return playerService.find(id)
    }

    @Post
    fun save(player: Player): Player {
        log.debug("save $player")
        return when (player.id) {
            null -> playerService.insert(player)
            else -> playerService.update(player)
        }
    }

    @Delete("/{id}")
    fun delete(@PathVariable id: String) {
        log.debug("delete player id=$id")
        return playerService.delete(id)
    }

}
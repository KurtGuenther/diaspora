package diaspora.service

import javax.inject.Singleton
import kotlin.random.Random

@Singleton
class NamingService {

    private val clusters = listOf(
            "Dreyer nebula",
            "M104",
            "M81",
            "Triangulum",
            "Andromeda",
            "Magellanic Cloud",
            "Ursa Cluster",
            "Canis Major",
            "Canis Minor",
            "Circinus",
            "Starburst",
            "Flocculent Spiral",
            "Centaurus"
    )

    private val systems = listOf(
            "Sirius",
            "Canopus",
            "Arcturus",
            "Alpha Centauri A",
            "Vega",
            "Rigel",
            "Procyon",
            "Achernar",
            "Betelgeuse",
            "Hadar",
            "Agena",
            "Capella",
            "Altair",
            "Aldebaran",
            "Spica",
            "Antares",
            "Pollux",
            "Fomalhaut",
            "Deneb"
    )

    fun clusterName() = selectFrom(clusters)

    fun systemName() = selectFrom(systems)

    private fun selectFrom(list: List<String>): String = list[Random.nextInt(list.size)]
}
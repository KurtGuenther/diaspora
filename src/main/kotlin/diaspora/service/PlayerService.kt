package diaspora.service

import diaspora.domain.Player
import diaspora.persistence.PlayerDataService
import javax.inject.Singleton

@Singleton
class PlayerService(val playerDataService: PlayerDataService) {

    fun find(id: String): Player {
        return playerDataService.find(id)
    }

    fun insert(player: Player): Player {
        return playerDataService.insert(player)
    }

    fun update(player: Player): Player {
        playerDataService.update(player)
        return player
    }

    fun delete(id: String) {
        playerDataService.delete(id)
    }

}
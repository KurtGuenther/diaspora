package diaspora.service

import diaspora.domain.*
import diaspora.utils.Common.fromZero4df
import diaspora.utils.Common.fudge4df
import diaspora.utils.Common.generateId
import diaspora.utils.Common.unnamed
import javax.inject.Singleton
import kotlin.random.Random

@Singleton
class GameService(val namingService: NamingService) {

    fun createGame(
            players: List<Player>,
            name: String = unnamed,
            notes: String = ""
    ): Game {

        val clusterSize = players.size + Random.nextInt(players.size)


        return Game(
                id = generateId(),
                name = name,
                notes = notes,
                players = players,
                cluster = createCluster(systemsSize = clusterSize),
                caller = null,
                referee = null
        )

    }


    fun createCluster(
            name: String = namingService.clusterName(),
            notes: String = "",
            systemsSize: Int = fromZero4df() + 2
    ): Cluster {

        return Cluster(
                id = generateId(),
                name = name,
                notes = "",
                starSystems = (0..systemsSize).map { createSystem(name) }.toSet(),
                slipStreams = emptySet()
        )

    }

    fun createSystem(
            name: String = namingService.systemName(),
            notes: String = ""
    ): StarSystem {
        return StarSystem(
                id = generateId(),
                name = name,
                notes = notes,
                statistics = "Unknown",
                aspects = "Unknown",
                details = "Unknown",
                planets = createPlanets(name),
                technology = fudge4df(),
                environment = fudge4df(),
                resources = fudge4df()
        )
    }

    fun createPlanets(
            systemName: String,
            number: Int = fudge4df().value + 4
    ): List<Planet> = when (number) {
        0 -> emptyList()
        else ->
            createPlanets(systemName, number - 1) +
                    Planet(
                            id = generateId(),
                            name = "$systemName $number",
                            notes = "",
                            habitability = fudge4df()
                    )
    }

}
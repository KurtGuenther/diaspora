package diaspora.persistence

import org.bson.types.ObjectId

object DataUtils {


    internal fun mapId(id: String?) = when {
        id.isNullOrBlank() -> ObjectId()
        else -> id
    }
}
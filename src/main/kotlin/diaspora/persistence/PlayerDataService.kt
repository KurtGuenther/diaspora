package diaspora.persistence

import com.mongodb.client.MongoClient
import com.mongodb.client.MongoCollection
import diaspora.domain.Player
import diaspora.persistence.MongoCommon.queryById
import mu.KotlinLogging
import org.bson.BsonDateTime
import org.bson.BsonObjectId
import org.bson.Document
import org.bson.types.ObjectId
import java.util.*
import javax.inject.Singleton

private val log = KotlinLogging.logger {}

interface PlayerDataService {

    fun find(id: String): Player

    fun insert(player: Player): Player

    fun update(player: Player): Player

    fun delete(id: String)
}

// FUTURE: this could be generated with kapt
@Singleton
class PlayerDataServiceImpl(val mongoClient: MongoClient) : PlayerDataService {

    override fun find(id: String): Player {
        val document = getCollection().find(queryById(id)).first()
                ?: throw NotFoundException("No Player with id: $id")
        val player = read(document)
        log.debug { "Found $player" }
        return player
    }

    // FUTURE - add optimistic locking transactional semantics using query on timestamp
    override fun insert(player: Player): Player {
        val value = getCollection().insertOne(write(player)).insertedId ?: throw DataServiceException("no id generated")
        val id = value.asObjectId().value.toHexString() ?: throw DataServiceException("Internal No ID provided")
        val inserted = player.toBuilder().setId(id).build()
        log.debug { "Inserted $inserted" }
        return inserted
    }

    override fun update(player: Player): Player {
        val target = player.toBuilder().setTimeStamp(Date()).build()
        val query = queryById(target.id ?: throw DataServiceException("Id cannot be null for update"))
        val result = getCollection().findOneAndReplace(query, write(target))
        log.debug { "Updated $target" }
        return target
    }

    override fun delete(id: String) {
        getCollection().deleteOne(Document("_id", id))
    }

    internal fun getCollection(): MongoCollection<Document> {
        return mongoClient
                .getDatabase("diaspora")
                .getCollection("players")
    }

    internal fun read(document: Document): Player {
        @Suppress("UNCHECKED_CAST")
        return Player(
                id = (document["_id"] as ObjectId).toHexString(),
                timestamp = document["timestamp"] as Date,
                name = document["name"] as String,
                notes = document["notes"] as String,
                systemIds = document["systemIds"] as List<String>
        )
    }

    internal fun write(player: Player): Document {
        val doc = Document()
        doc.append("_id", BsonObjectId(if (player.id == null) ObjectId() else ObjectId(player.id)))
        doc.append("timestamp", BsonDateTime(System.currentTimeMillis()))
        doc.append("name", player.name)
        doc.append("notes", player.notes)
        doc.append("systemIds", player.systemIds)
        return doc
    }
}
package diaspora.persistence

import com.mongodb.client.model.FindOneAndReplaceOptions
import com.mongodb.client.model.ReturnDocument
import org.bson.BsonObjectId
import org.bson.Document
import org.bson.types.ObjectId

class NotFoundException(val msg: String = "", val e: Exception? = null) : DataServiceException(msg, e)

open class DataServiceException(val msg1: String = "", val e1: Exception? = null) : Exception(msg1, e1)


class MongoDataService {
    companion object {
    }


}

object MongoCommon {
    /**
     * this is an option that returns the updated object
     */
    val returnDocAfterReplace = FindOneAndReplaceOptions().returnDocument(ReturnDocument.AFTER)

    /**
     * this generates a query based on the ID
     *
     * @hexString - 20 byte hex string as used by org.bson.ObjectId
     * @return document ready to put into a find or findAndUpdate
     */
    fun queryById(hexString: String) = Document("_id", BsonObjectId(ObjectId(hexString)))

    fun wrapId(hexString: String?) = BsonObjectId(if (hexString == null) ObjectId() else ObjectId(hexString))

}
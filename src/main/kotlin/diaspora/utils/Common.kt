package diaspora.utils

import diaspora.domain.Fudge
import java.util.*
import kotlin.random.Random

object Common {

    val unnamed = "[unnamed]"

    /**
     * generate a random, but unique ID.
     *
     * @return UUID in string form
     */
    fun generateId() = UUID.randomUUID().toString()

    /**
     * simulate a roll of 4 dice
     *
     * @return  instance of Fudge
     */
    fun fudge4df(): Fudge = Fudge.values()[roll(4, 0) + 4]

    /**
     * @return a non-negative distribution around 4 which ranges from 0-8.   This
     * is useful if you need a small collection of items.
     */
    fun fromZero4df(): Int = fudge4df().value + 4

    internal fun roll(rollsLeft: Int, sum: Int): Int = when (rollsLeft) {
        0 -> sum
        else -> roll(rollsLeft - 1, sum + roll())
    }

    /**
     * @return a random number of values -1, 0, and 1
     */
    internal fun roll(): Int = Random.nextInt(3) - 1

}
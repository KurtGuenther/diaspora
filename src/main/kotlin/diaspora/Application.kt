package diaspora

import io.micronaut.runtime.Micronaut.*

typealias Pass = Unit

fun main(args: Array<String>) {
    build()
            .args(*args)
            .eagerInitConfiguration(true)
            .packages("diaspora", "diaspora.service")
            .start()
}

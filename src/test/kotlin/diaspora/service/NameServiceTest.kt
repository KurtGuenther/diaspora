package diaspora.service

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.ints.shouldBeGreaterThan

class NameServiceTest : StringSpec ({

    val nameService = NamingService()

    "generate cluster names" {
        val set = mutableSetOf<String>()
        for (i in 1..100) {
            set.add(nameService.clusterName())
        }
        set.size shouldBeGreaterThan 1
    }

    "generate system names" {
        val set = mutableSetOf<String>()
        for (i in 1..100) {
            set.add(nameService.systemName())
        }
        set.size shouldBeGreaterThan 1
    }

})
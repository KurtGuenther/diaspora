package diaspora.service

import diaspora.domain.Player
import diaspora.utils.Common.generateId
import diaspora.utils.Common.unnamed
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.ints.shouldBeGreaterThanOrEqual
import io.kotest.matchers.ints.shouldBeLessThanOrEqual
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk

class GameServiceTest : StringSpec({

    val namingService = mockk<NamingService>()
    every { namingService.clusterName() } returns "cluster"
    every { namingService.systemName() } returns "system"

    val sut = GameService(namingService)

    "create a system" {
        val system = sut.createSystem("alpha")
        system.name shouldBe "alpha"
        system.planets.size shouldBeGreaterThanOrEqual 0
        system.planets.size shouldBeLessThanOrEqual 8
    }

    "create a cluster" {
        val cluster = sut.createCluster(unnamed)
        cluster.name shouldBe unnamed
    }

    "create a game" {
        val players = listOf("Kurt", "Mike", "Dave", "CJay")
                .map {
                    Player(
                            id = generateId(),
                            name = it,
                            notes = "",
                            systemIds = emptyList()
                    )
                }.toList()

        val game = sut.createGame(players)
        game.caller shouldBe null
        game.referee shouldBe null
    }

})
package diaspora.controller

import diaspora.service.NamingService
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk


class NamingControllerTest : StringSpec({

    val namingService = mockk<NamingService>()
    every { namingService.clusterName() } returns "cluster"
    every { namingService.systemName() } returns "system"
    val sut = NamingController(namingService)

    "test the cluster naming controller" {
        sut.clusterName().name shouldBe "cluster"
    }

    "test the system naming controller" {
        sut.systemName().name shouldBe "system"
    }

})


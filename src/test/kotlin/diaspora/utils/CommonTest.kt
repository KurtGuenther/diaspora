package diaspora.utils

import diaspora.Pass
import diaspora.domain.Fudge
import diaspora.utils.Common.fudge4df
import diaspora.utils.Common.generateId
import diaspora.utils.Common.roll
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.comparables.shouldNotBeEqualComparingTo
import io.kotest.matchers.ints.shouldBeLessThan
import io.kotest.matchers.shouldBe

class CommonTest : StringSpec({

    "test generate key" {
        generateId() shouldNotBeEqualComparingTo generateId()
    }

    "test rolling a single die" {
        var foundZero = false
        var foundMinusOne = false
        var foundOne = false
        for (i in 1..1000) {
            when (roll()) {
                0 -> foundZero = true
                -1 -> foundMinusOne = true
                1 -> foundOne = true
                else -> throw Exception("not in bounds")
            }
        }
        // Note: that the chances of this failing only approaches zero.
        foundZero shouldBe true
        foundMinusOne  shouldBe true
        foundOne shouldBe true
    }

    "test a full 4df roll with 4 die" {
        var mediocreCount = 0
        var terribleCount = 0
        var superbCount = 0

        for (i in 1..1000) {
            when (fudge4df()) {
                Fudge.Terrible -> terribleCount++
                Fudge.Mediocre -> mediocreCount++
                Fudge.Superb -> superbCount++
                else -> Unit
            }
        }

        // Note: the probability of a failure only approaches zero.
        terribleCount shouldBeLessThan mediocreCount
        superbCount shouldBeLessThan mediocreCount
    }

})